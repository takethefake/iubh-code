# Part 1: Building the Thread home page

This exercise asks you to take a design and build it in the browser with HTML and CSS. No JavaScript should be used in this part of the exercise. You do not need to worry about making the design responsive, or supporting multiple browsers. Focus on the desktop experience only for the purposes of this test.

The design has been uploaded to InvisionApp, which lets you easily see measurements and information on the design. To access it, follow this link:

https://projects.invisionapp.com/d/main/default/#/console/14353070/298525871/inspect

And log in with these details:

- username: jack+threadcandidate@thread.com
- password: DressWell123
