import React from "react";
import { Message } from "./components/Message";

function App() {
  return (
    <div className="App">
      <Message>Hello World</Message>
      <Message messageType="error">Hello World</Message>
      <Message messageType="warning">Hello World</Message>
    </div>
  );
}

export default App;
