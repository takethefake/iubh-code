import * as React from "react";
import styles from "./Message.module.css";

type MessageProps = {
  messageType?: "info" | "warning" | "error";
};

export const Message: React.FC<MessageProps> = ({
  messageType = "info",
  children,
}) => {
  return <div className={`${styles[messageType]} message`}>{children}</div>;
};
