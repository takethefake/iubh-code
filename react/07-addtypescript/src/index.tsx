import * as React from 'react'
import ReactDOM from 'react-dom'

const rootElement = document.getElementById('root')
const Message:React.FC = ({children})=> <div className="message">{children}</div>

ReactDOM.render(<Message>Test</Message>,rootElement)
