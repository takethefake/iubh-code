import React, { ChangeEventHandler } from "react";

export function useSiteTitle(initialSiteTitle: string) {
  const [siteTitle, setSiteTitle] = React.useState(initialSiteTitle);

  React.useEffect(() => {
    window.document.title = siteTitle;
    console.log("effect:", siteTitle);
    return () => {
      console.log("unmount Use Effect");
      window.document.title = "unmounted";
    };
  }, [siteTitle]);

  return { setSiteTitle, siteTitle };
}
