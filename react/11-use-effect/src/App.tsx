import React, { ChangeEventHandler } from "react";
import { useSiteTitle } from "./hooks/useSiteTitle";

function App() {
  const [showEffect, setShowEffect] = React.useState(false);
  const toggleEffect = () => {
    setShowEffect((currenShowEffect) => !currenShowEffect);
  };
  return (
    <div className="App">
      <button onClick={toggleEffect}>Toggle Show Effect</button>
      {showEffect && <UseEffect />}
      {showEffect && <UseEffectButton />}
    </div>
  );
}

export const UseEffect = () => {
  const { setSiteTitle, siteTitle } = useSiteTitle("Initial SiteTitle");
  console.log("before Render:", siteTitle);

  const onChangeSiteTitle: ChangeEventHandler<HTMLInputElement> = (e) => {
    console.log("onchange:", e.currentTarget.value);
    setSiteTitle(e.currentTarget.value);
  };
  return <input onChange={onChangeSiteTitle} value={siteTitle} />;
};

export const UseEffectButton = () => {
  const { setSiteTitle, siteTitle } = useSiteTitle("Initial SiteTitle");
  console.log("before Render:", siteTitle);
  return (
    <>
      <button
        onClick={(e) => {
          setSiteTitle("a");
        }}
      >
        Site Title a
      </button>
      <button
        onClick={(e) => {
          setSiteTitle("b");
        }}
      >
        Site Title B
      </button>
    </>
  );
};

export default App;
