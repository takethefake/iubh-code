import React, { ChangeEventHandler } from "react";

export interface JokeResponse {
  type: "success" | "error";
  value: Joke;
}

export interface Joke {
  id: number;
  joke: string;
  categories: string[];
}

function App() {
  const [isLoading, setIsLoading] = React.useState<boolean>(false);
  const [joke, setJoke] = React.useState<Joke | null>(null);

  const fetchJoke = async () => {
    setIsLoading(true);
    const jokeResponse = await fetch("http://api.icndb.com/jokes/random");
    console.log("response", jokeResponse);
    const jokeJson = (await jokeResponse.json()) as JokeResponse;

    if (jokeResponse.status !== 200 || jokeJson.type !== "success") {
      setJoke(null);
      setIsLoading(false);
      return;
    }
    console.log("JSON response", jokeJson);
    setJoke(jokeJson.value);
    setIsLoading(false);
  };

  React.useEffect(() => {
    fetchJoke();
  }, []);

  return (
    <div className="App">
      {isLoading && <div>Loading...</div>}
      {!isLoading && joke !== null && <h1>{joke.joke}</h1>}
      <button onClick={fetchJoke}>New Joke</button>
    </div>
  );
}

export default App;
