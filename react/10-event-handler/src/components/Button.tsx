import * as React from "react";

type ButtonProps = {
  onClick?: React.MouseEventHandler<HTMLButtonElement>;
};

export const Button: React.FC<ButtonProps> = ({ onClick, children }) => {
  const onInternalClick: React.MouseEventHandler<HTMLButtonElement> = (e) => {
    if (onClick !== undefined) {
      onClick(e);
    } else {
      alert("internal alert");
    }
  };
  return <button onClick={onInternalClick}>{children}</button>;
};
