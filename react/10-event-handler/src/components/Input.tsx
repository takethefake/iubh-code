import * as React from "react";

export const Input = () => {
  let [inputValue, setInputValue] = React.useState("");
  const onChange: React.ChangeEventHandler<HTMLInputElement> = (e) => {
    console.log(e.currentTarget.value);
    setInputValue(e.currentTarget.value);
    console.log("inputValue", inputValue); // inputValue ist noch nicht der neue Wert
  };
  return <input onChange={onChange} value={inputValue} />;
};
