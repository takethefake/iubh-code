import * as React from "react";

export const CounterButtton = () => {
  const [count, setCount] = React.useState(0);

  const onClick = () => {
    console.log("onClick");
    setTimeout(() => {
      console.log("setTimeout count");
      setCount((count) => count + 1);
    }, 2000);
  };

  return <button onClick={onClick}>{count}</button>;
};
