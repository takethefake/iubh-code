import React from "react";
import { Button } from "./components/Button";
import { Input } from "./components/Input";
import { CounterButtton } from "./components/CounterButton";

function App() {
  const onClick = () => {
    alert("APP CLICK");
  };
  return (
    <div className="App">
      <Button>Internal Click</Button>
      <Button onClick={onClick}>App Click</Button>
      <Input />
      <CounterButtton />
    </div>
  );
}

export default App;
