import React from "react";
import { Message } from "./components/Message";
import { StyledMessage } from "./components/StyledComponentsMessage";

function App() {
  return (
    <div className="App">
      <Message>Hello World</Message>
      <StyledMessage messageType="error">Hello World</StyledMessage>
      <StyledMessage messageType="warning">Hello World</StyledMessage>
    </div>
  );
}

export default App;
