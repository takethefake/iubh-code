import * as React from "react";

type MessageProps = {
  messageType?: "info" | "warning" | "error";
};

export const Message: React.FC<MessageProps> = ({
  messageType = "info",
  children,
}) => {
  const colors = {
    info: "blue",
    warning: "yellow",
    error: "red",
  };

  return (
    <div className={` message`} style={{ color: colors[messageType] }}>
      {children}
    </div>
  );
};
