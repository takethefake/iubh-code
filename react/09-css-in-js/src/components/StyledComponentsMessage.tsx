import * as React from "react";
import styled from "styled-components";

export const StyledMessage = styled.div<{
  messageType?: "info" | "warning" | "error";
}>`
  color: ${(props) =>
    props.messageType === "info"
      ? "blue"
      : props.messageType === "warning"
      ? "yellow"
      : "red"};
`;
