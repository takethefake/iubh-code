import * as React from "react";
import { MessageProps } from "../components/Message";
import { useContext } from "react";

interface MessageContext {
  messages: MessageProps[];
}

export const messageContext = React.createContext<MessageContext>({
  messages: [],
});

const { Provider } = messageContext;

export const MessageProvider: React.FC<{ messages?: MessageProps[] }> = ({
  messages,
  children,
}) => {
  const passedMessages = messages
    ? messages
    : ([
        { messageTitle: "Hallo", messageType: "info" },
        {
          messageTitle: "hier werden Props noch durchgereicht",
          messageType: "error",
        },
      ] as MessageProps[]);
  return <Provider value={{ messages: passedMessages }}>{children}</Provider>;
};

export function useMessage() {
  return useContext(messageContext);
}
