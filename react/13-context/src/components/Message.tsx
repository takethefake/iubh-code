import * as React from "react";
import styles from "./Message.module.css";

export type MessageProps = {
  messageType?: "info" | "warning" | "error";
  messageTitle: string;
};

export const Message: React.FC<MessageProps> = ({
  messageType = "info",
  messageTitle,
}) => {
  return <div className={`${styles[messageType]} message`}>{messageTitle}</div>;
};
