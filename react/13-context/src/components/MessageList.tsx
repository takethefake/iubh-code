import { Message, MessageProps } from "./Message";
import { useMessage } from "../provider/MessageProvider";

export const MessageList = () => {
  const { messages } = useMessage();
  return (
    <div className="message-list">
      {messages.map((messageProps) => (
        <Message {...messageProps} />
      ))}
    </div>
  );
};
