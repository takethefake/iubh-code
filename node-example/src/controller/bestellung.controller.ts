import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { Bestellung, bestellungSchema } from "../entity/bestellung.model";

export const getBestellung = async (
  _: Request,
  res: Response
): Promise<void> => {
  const bestellungRepository = await getRepository(Bestellung);

  const bestellung = await bestellungRepository.find();
  res.send(bestellung);
};

export const postBestellung = async (
  req: Request,
  res: Response
): Promise<void> => {
  const bestellungRepository = await getRepository(Bestellung);
  const bestellung = new Bestellung();

  try {
    const validatedBody = await bestellungSchema.validate(req.body);
    bestellung.kundennummer = validatedBody.kundennummer;
    // @ts-expect-error-its-no-real-pizza-bestellung
    bestellung.pizzaBestellungen = validatedBody.items.map((item) => {
      return {
        pizza: item.id,
        anzahl: item.anzahl,
      };
    });
    const newBestellung = await bestellungRepository.save(bestellung);
    res.send(newBestellung);
  } catch (e) {
    if (e.name === "ValidationError" && e.errors) {
      res.send(e.errors);
    } else {
      res.send({ error: e.message });
    }
  }
};
